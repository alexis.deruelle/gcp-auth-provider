import os

import certifi
import urllib3
from starlette.exceptions import HTTPException

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED", ca_certs=certifi.where())

JWT_TOKEN = os.environ.get("GCP_JWT") or os.environ.get("CI_JOB_JWT_V2")


def get_iam_credentials(service_account, federated_token):
    resp = urllib3.request(
        method="POST",
        url=f"https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/{service_account}:generateAccessToken",
        headers={
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f"Bearer {federated_token}",
        },
        json={"scope": ["https://www.googleapis.com/auth/cloud-platform"]},
    )
    if resp.status != 200:
        raise HTTPException(
            status_code=500,
            detail=f"Failed to get iam credential token for service_account={service_account} msg: {resp.json()}",
        )
    return resp.json()["accessToken"]


def get_sts_token(audience):
    if not JWT_TOKEN:
        raise HTTPException(
            status_code=401, detail="Missing $CI_JOB_JWT_V2 or $GCP_JWT token"
        )

    resp = urllib3.request(
        method="POST",
        url="https://sts.googleapis.com/v1/token",
        headers={"Accept": "application/json", "Content-Type": "application/json"},
        json={
            "audience": audience,
            "grantType": "urn:ietf:params:oauth:grant-type:token-exchange",
            "requestedTokenType": "urn:ietf:params:oauth:token-type:access_token",
            "scope": "https://www.googleapis.com/auth/cloud-platform",
            "subjectTokenType": "urn:ietf:params:oauth:token-type:jwt",
            "subjectToken": JWT_TOKEN,
        },
    )
    if resp.status != 200:
        raise HTTPException(
            status_code=500,
            detail=f"Failed to get sts token for audience={audience} msg: {resp.json()}",
        )
    return resp.json()["access_token"]
