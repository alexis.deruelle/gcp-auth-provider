FROM registry.hub.docker.com/library/python:3.12-alpine

ENV PORT=80
WORKDIR /code

COPY pyproject.toml poetry.lock README.md /code/
COPY ./gcp_auth_provider /code/gcp_auth_provider

RUN apk upgrade --no-cache  \
    && pip install --no-cache-dir .

EXPOSE ${PORT}
# hadolint ignore=DL3025
CMD uvicorn gcp_auth_provider.main:app --host=0.0.0.0 --port=${PORT}
